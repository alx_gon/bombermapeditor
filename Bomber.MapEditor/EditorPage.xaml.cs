﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace Bomber.MapEditor
{
    /// <summary>
    /// Interaction logic for EditorPage.xaml
    /// </summary>
    public partial class EditorPage : Page
    {
        string[,] _map;
        CellButton[,] _uiMap;
        string _name;
        private string _tileSet;
        private LevelContentData _levelData;
        private int _cellWidth = 50;
        private int _cellHeight = 50;

        private (int, int)? _chooseStart;
        private (int, int)? _chooseEnd;

        private LinkedList<CellButton> _chosen = new LinkedList<CellButton>();

        public EditorPage(string path, string tileSet)
        {
            InitializeComponent();
            InitTileset(tileSet);

            _name = System.IO.Path.GetFileNameWithoutExtension(path);
            _tileSet = tileSet;

            _levelData = JsonConvert.DeserializeObject<LevelContentData>(System.IO.File.ReadAllText(path));

            var elems = _levelData.map.Aggregate((x, y) => x.Concat(y).ToArray());
            var tiles = tileList.Items.Cast<TileControl>();

            MakeMap(_levelData.map.Length, _levelData.map[0].Length);

            
            for(int i = 0; i < _levelData.map.Length; i++)
                for(int j = 0; j < _levelData.map[i].Length; j++)
                {
                    var elem = _levelData.map[i][j];
                    _map[i, j] = elem;

                    var tileCtrl = tiles.FirstOrDefault(x => (x.IdLabel.Content as string) == elem);

                    var img = new CellContent();
                    img.Source = tileCtrl?.Tile.Source;

                    if (img.Source == null || tileCtrl.Indexed)
                        img.Index = (tileList.SelectedItem as TileControl)?.Id;
                    _uiMap[i, j].Content = img;
                }
        }

        private void MakeMap(int width, int height)
        {
            _map = new string[width, height];
            _uiMap = new CellButton[width, height];

            for (var y = 0; y < height; y++)
            {
                var column = new ColumnDefinition();
                column.Width = new GridLength(_cellWidth, GridUnitType.Pixel);
                fieldGrid.ColumnDefinitions.Add(column);
            }
            for (var x = 0; x < width; x++)
            {
                var row = new RowDefinition()
                {
                    Height = new GridLength(_cellHeight, GridUnitType.Pixel)
                };
                fieldGrid.RowDefinitions.Add(row);
            }

            for (var y = 0; y < height; y++)
                for (var x = 0; x < width; x++)
                {
                    int tx = x;
                    int ty = y;

                    var button = new CellButton
                    {
                        Background = Brushes.LightGray,
                        X = x,
                        Y = y,
                        Fire =
                            (s) =>
                            {
                                var img = new CellContent();
                                var tileCtrl = (tileList.SelectedItem as TileControl);
                                img.Source = tileCtrl?.Tile.Source;

                                if (img.Source == null || tileCtrl.Indexed)
                                    img.Index = (tileList.SelectedItem as TileControl)?.Id;

                                s.Content = img;

                                _map[tx, ty] = (tileList.SelectedItem as TileControl)?.IdLabel.Content as string;
                            }
                            
                    };
                    fieldGrid.Children.Add(button);

                    button.Background = Brushes.Gray;

                    button.Click += (s, e) =>
                        {
                            //if (e.LeftButton != MouseButtonState.Pressed)
                            //    return;

                            var b = s as CellButton;
                            if (Keyboard.IsKeyDown(Key.LeftCtrl))
                            {
                                if (!_chosen.Contains(b))
                                {
                                    _chosen.AddLast(b);
                                    b.Background = Brushes.Yellow;
                                }
                                else
                                {
                                    b.Background = Brushes.Gray;
                                    _chosen = new LinkedList<CellButton>(_chosen.Where(z => z != s));
                                }

                                return;
                            }
                            else
                            {
                                foreach (var t in _chosen)
                                {
                                    t.Fire(t);
                                    t.Background = Brushes.Gray;
                                }
                                _chosen.Clear();
                            }
                            (s as CellButton).Fire(s as CellButton);
                        };

                    Grid.SetColumn(button, y);
                    Grid.SetRow(button, x);

                    _map[x, y] = "0";
                    _uiMap[x, y] = button;
                }
        }

        public EditorPage(string name, string tileSet, int width, int height)
        {
            InitializeComponent();
            _name = name;

            InitTileset(tileSet);
            MakeMap(width, height);
        }

        private void InitTileset(string tileSet)
        {
            var tileset = System.IO.File.ReadAllLines(tileSet)
                .Select(x => Tile.Parse(System.IO.Path.GetDirectoryName(tileSet), x))
                .ToArray();

            tileList.Items.Add(new TileControl("0", null));
            foreach (var t in tileset.Select(x => new TileControl(x.Id, x.Image) { Indexed = x.Indexed }))
                tileList.Items.Add(t);
            tileList.SelectedIndex = 0;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var map = new string[_map.GetLength(0)][];
            for (var y = 0; y < _map.GetLength(0); y++)
            {
                map[y] = new string[_map.GetLength(1)];
                for (var x = 0; x < _map.GetLength(1); x++)
                    map[y][x] = _map[y, x];
            }

            var content = _levelData ?? new LevelContentData()
            {
                name = _name,
                version = "1.0",
                map = map,
                other = new object()
            };

            _levelData.map = map;

            System.IO.File.WriteAllText(_name + ".json", JsonConvert.SerializeObject(content));
        }

        private void Page_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                foreach(var b in _chosen)
                {
                    b.Fire(b);
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < _map.GetLength(0); i++)
                for (int j = 0; j < _map.GetLength(1); j++)
                    if (i == 0 || j == 0 || i == _map.GetLength(0) - 1 || j == _map.GetLength(1) - 1)
                    {
                        _chosen.AddLast(_uiMap[i, j]);
                        _uiMap[i, j].Background = Brushes.Yellow;
                    }
        }
    }

    public class LevelContentData
    {
        public string name;
        public string version;
        public string[][] map;
        public object other;
    }

    public class CellButton : Button
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Action<CellButton> Fire { get; set; }
    }
}
