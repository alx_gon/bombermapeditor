﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace Bomber.MapEditor
{
    /// <summary>
    /// Interaction logic for WizardPage.xaml
    /// </summary>
    public partial class WizardPage : Page
    {
        private Frame _frame;
        private string _tileSet;
        private OpenFileDialog _dialog = new OpenFileDialog();

        public WizardPage(Frame frame)
        {
            InitializeComponent();
            _frame = frame;
            _tileSet = System.IO.Path.GetFullPath("./default.txt");
            _dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            _dialog.FileName = _tileSet;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var sizeString = sizeBox.Text;

                var spl = sizeString.Split('x');

                if (spl.Length != 2) return;
                if (_tileSet == null) return;

                var x = int.Parse(spl[0]);
                var y = int.Parse(spl[1]);

                _frame.Navigate(new EditorPage(nameBox.Text, _tileSet, x, y));
            }
            catch
            {

            }
        }

        private void tileSet_Click(object sender, RoutedEventArgs e)
        {
            if(_dialog.ShowDialog() == DialogResult.OK)
            {
                _tileSet = _dialog.FileName;
            }
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            if (_tileSet == null) return;
            if (_dialog.ShowDialog() == DialogResult.OK)
                _frame.Navigate(new EditorPage(_dialog.FileName, _tileSet));
        }
    }

    public class Tile
    {
        public Tile(BitmapImage image, string id)
        {
            Image = image;
            Id = id;
        }

        public static Tile Parse(string s)
        {
            return Parse("", s);
        }

        public static Tile Parse(string parent, string s)
        {
            var spl = s.Split(':');
            if (spl.Length == 1 || spl[1].Length == 1)
                return Load(spl[0]);
            if (spl.Length == 3 && spl[2].Equals("+"))
                return Load(spl[0], parent + spl[1], true);
            return Load(spl[0], parent + spl[1], false);
        }

        public static Tile Load(string id, bool indexed = false)
        {
            return new Tile(null, id) { Indexed = indexed };
        }

        public static Tile Load(string id, string tilePath, bool indexed)
        {
            var uri = new Uri(tilePath);
            var bmp = new BitmapImage(uri);

            return new Tile(bmp, id)
            {
                Indexed = indexed
            };
        }

        public BitmapImage Image { get; }
        public string Id { get; }
        public bool Indexed { get; set; }
    }
}
