﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Bomber.MapEditor
{
    /// <summary>
    /// Interaction logic for CellContent.xaml
    /// </summary>
    public partial class CellContent : UserControl
    {
        public CellContent()
        {
            InitializeComponent();
        }

        public void Reset()
        {
            Source = null;
            indexLabel.Content = "";
        }

        public ImageSource Source
        {
            set
            {
                tileImage.Source = value;
            }
            get
            {
                return tileImage.Source;
            }
        }

        public string Index
        {
            set
            {
                indexLabel.Content = value;
            }
        }
    }
}
